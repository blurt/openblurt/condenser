# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Features:
- Added changelog
- created old-master branch
- rolled master to before @faddat attempted to remove dependencies on blurt.buzz APIs
- 


### Fixes:

## [0.0.1] - 2021-02-14





